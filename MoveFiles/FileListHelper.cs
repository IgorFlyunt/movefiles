﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace MoveFiles
{
    public class FileListHelper
    {
        public string MoveFolder;
        public static string FileMask = "*.*";
        public static string CopyFolderName = "Files copy";



        /// <summary>
        /// Set path to folder for moving inner files
        /// </summary>
        /// <param name="moveFolder"></param>
        public FileListHelper(string moveFolder)
        {
            this.MoveFolder = moveFolder;
        }


        /// <summary>
        /// Set path to folder for moving inner files
        /// </summary>
        /// <param name="folder">Folder path</param>
        public void SetPathToFolder(string folder)
        {
            this.MoveFolder = folder;
        }

        /// <summary>
        /// //Open folder and get all files from folder and subfolders to the list
        /// </summary>
        /// <returns></returns>
        public List<string> GetFiles()
        {
            try
            {
                List<String> MoveFile = Directory.GetFiles(MoveFolder, 
                    FileListHelper.FileMask, SearchOption.AllDirectories).ToList();
                return MoveFile;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        /// <summary>
        /// Creates new folder for copied files inside of specified folder
        /// </summary>
        /// <param name="folder">Folder path</param>
        /// <returns>Path of new folder</returns>
        public string CreateCopyFolder(string folder)
        {

            if (Directory.Exists(folder))
            {
                var copyFolder = Directory.CreateDirectory(Path.Combine(folder, CopyFolderName));

                return copyFolder.ToString();
            }

            return null;

        }

        /// <summary>
        /// Move all found files to Copy Folder
        /// </summary>
        /// <param name="newFolder"></param>
        public int MoveFiles(string newFolder)
        {
            int count = 0;
            List<string> files = Directory.GetFiles(MoveFolder,
                FileListHelper.FileMask, SearchOption.AllDirectories).ToList();

            //Move all files to new folder
            foreach (string file in files)
            {
                FileInfo mFile = new FileInfo(file);
                var path = newFolder + "\\" + mFile.Name;
                var fileExists = new FileInfo(path).Exists;
                if (!fileExists)
                {
                    mFile.CopyTo(path);
                    count++;
                }

            }

            return count;
        }

        /// <summary>
        ///  Delete files from source folder - MoveFolder
        /// </summary>
        public int DeleteFiles()
        {
            int count = 0;
            List<string> files = Directory.GetFiles(MoveFolder,
                FileListHelper.FileMask, SearchOption.TopDirectoryOnly).ToList();

            if (files.Count <= 0) return count;
            foreach (string file in files)
            {
                try
                {
                    File.Delete(file);
                    count++;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error on deleting {file}. {e.Message}");
                }

            }

            return count;
        }

    }


}
