﻿using System;
using System.IO;

namespace MoveFiles
{
    class Program
    {

        static void Main(string[] args)
        {

            /*
             * Task :
             * take some "source" folder path as an input data
             * based on folder path, the app should get the list nested files
             * create a new folder in "source" folder (you can call it *"Files copy"*)
             * move all found files to the newly created folder
             * remove all files from previous locations
             * Unit test
           */



            try
            {
                Console.WriteLine("Enter the path to the folder :");
                var moveFolder = Console.ReadLine();
                var fileHelper = new FileListHelper(moveFolder);
               
                var folderFilesCopy = fileHelper.CreateCopyFolder(fileHelper.MoveFolder);
               
                fileHelper.MoveFiles(folderFilesCopy);

                fileHelper.DeleteFiles();
                
                Console.WriteLine("Done, your files have been moved to :" + Path.GetFullPath(folderFilesCopy.ToString()));
            }
            
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
