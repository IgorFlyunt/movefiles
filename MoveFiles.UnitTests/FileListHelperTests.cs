﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MoveFiles.UnitTests
{

    [TestFixture]
    public class FileListHelperTests
    {
        private string testFolder;
        private string testFolder2;
        private string testFolder3;



        private FileListHelper fileHelper;

        [SetUp]
        public void SetUp()
        {
         
            var currentDirectory = Directory.GetCurrentDirectory();
            
            testFolder = Path.Combine(currentDirectory, "MoveFile");
            testFolder2 = Path.Combine(currentDirectory, "MoveFile2");
            testFolder3 = Path.Combine(currentDirectory, "MoveFile3");

            fileHelper = new FileListHelper(testFolder);

            if (Directory.Exists(Path.Combine(currentDirectory, "MoveFile3")))
            {
                File.Delete(Path.Combine(testFolder3, "file"));
                File.Delete(Path.Combine(testFolder3, "file1"));
                File.Delete(Path.Combine(testFolder3, "file12"));
            }

            Directory.CreateDirectory(testFolder);
            Directory.CreateDirectory(testFolder2);
            Directory.CreateDirectory(testFolder3);

            //Create test files in testFolder
            string FileName = "\\file";
            string folder = testFolder + FileName;
            string line = "Test information line in file";
            int FilesCount = 3;
            int i = 0;

            while (i < FilesCount)
            {
                File.WriteAllText(folder, line);
                i++;
                folder += i.ToString();
            }
            
        }

        [Test]
        public void SetPathToFolderIsSamePathTest()
        {
            fileHelper.SetPathToFolder(testFolder);
            var folder = fileHelper.MoveFolder;

            Assert.That(folder, Is.SamePath(testFolder));
        }

        [Test]
        public void GetListFilesExistingFolderTest()
        {
            var expectedCount = 3;
            var files = fileHelper.GetFiles();
            Assert.AreEqual(files.Count, expectedCount);

            var filePrefix = "file";
            foreach (var file in files)
            {
                Assert.That(file.StartsWith(testFolder + "\\" + filePrefix));
            }

        }

        [Test]
        [TestCase(@"C:\TestTask\NotExistingFolder")]
        public void GetListFilesNotExistingFolderTest(string path)
        {
            fileHelper.MoveFolder = path;
            Assert.Throws<DirectoryNotFoundException>(() =>
                {
                    //code which must throws exception
                    fileHelper.GetFiles();
                }
                );
        }

        [Test]
        public void CreateCopyFolderExistingFolderTest()
        {
           
            var newFolder = fileHelper.CreateCopyFolder(testFolder);
            Assert.AreEqual(true, Directory.Exists(newFolder));
        }


        [Test]
        [TestCase(@"C:\TestTask\NotExistingFolder")]
        public void CreateCopyFolderNotExistingFolderTest(string path)
        {
            fileHelper.MoveFolder = path;

            var newFolder = fileHelper.CreateCopyFolder(path);

            Assert.AreEqual(null, newFolder);
        }

        [Test]
        public void MoveFilesMoveExistingFiles()
        {
            
            //Get Files
            var files = fileHelper.GetFiles();

            //Сopy files
            var count = fileHelper.MoveFiles(testFolder3);

            Assert.AreEqual(files.Count, count);

        }

        [Test]
        public void DeleteFilesDeleteExistingFiles()
        {
            
            //Get Files
            var files = fileHelper.GetFiles();

            Assert.AreEqual(files.Count, 3);

            //Delete Files
            var count = fileHelper.DeleteFiles();

            files = fileHelper.GetFiles();

            Assert.AreEqual(files.Count, 0);
            
        }
        
    }
}
